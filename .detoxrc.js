/** @type {Detox.DetoxConfig} */
module.exports = {
  testRunner: {
    args: {
      '$0': 'jest',
      config: 'e2e/jest.config.js'
    },
    jest: {
      setupTimeout: 120000
    }
  },
  artifacts: {
    rootDir: "./report"
  },
  apps: {
    'ios.debug': {
      type: 'ios.app',
      binaryPath: 'sut/Build/Products/Debug-iphonesimulator/Wikipedia.app',
      build: 'xcodebuild -project sut/Wikipedia.xcodeproj -scheme Wikipedia -configuration Debug -sdk iphonesimulator -derivedDataPath ios/build'
    },
    'ios.release': {
      type: 'ios.app',
      binaryPath: 'sut/Build/Products/Debug-iphonesimulator/Wikipedia.app',
      build: 'xcodebuild -project sut/Wikipedia.xcodeproj -scheme Wikipedia -configuration Release -sdk iphonesimulator -derivedDataPath ios/build'
    }
  },
  devices: {
    simulator: {
      type: 'ios.simulator',
      device: {
        type: 'iPhone 12'
      }
    }
  },
  configurations: {
    'ios.sim.debug': {
      device: 'simulator',
      app: 'ios.debug'
    },
    'ios.sim.release': {
      device: 'simulator',
      app: 'ios.release'
    }
  }
};
