[![Test](https://img.shields.io/badge/DETOX-323330?style=for-the-badge&logo=detox)](https://)
[![Test](https://img.shields.io/badge/mocha.js-323330?style=for-the-badge&logo=mocha&logoColor=Brown)](https://)
[![Language - Javascript](https://img.shields.io/badge/Javascript-F9DE03?style=for-the-badge&logo=javascript&logoColor=white)](https://)
[![mobile - ios](https://img.shields.io/badge/iOS-000000?style=for-the-badge&logo=apple&logoColor=white)](https://)

<br/>
<br/>
<div align="center">
    <img src="assets/DetoxLogo.png" alt="Logo" width="80" height="80">
    <h1 align="center"><strong>Demo Detox</strong></h1>
</div>
  <div align="center">
     Testing mobile apps
    <br />
  </div>



<br>
<br>



## Illustrations 
![illustration](assets/screenshot1.png)


## Description 
Small project to discover main features of Detox on the iOS Wikipedia app.
* Environment Setup
  * Tools
  * Init Detox and SUT
* E2E test implementation
  * Running detox test
  * Accessibility identifier
  * Manage multiple element in UI
  * Video recording
  * Test report


## Launch tests
- Launch the system under test on xcode 
  ![illustration](assets/screenshot2.png)

* When build succeed, run the app
 ![illustration](assets/screenshot3.png)
  => the simulator is up
  ![illustration](assets/screenshot1.png)

- Launch detox test
  ```
  npm run d:test
  ```

    ```
    # Run test with screenshots and video recording
    npm run d:video
    ```

## Add accessibility identifier 
* Switch to Debug View Hierarchy
![illustration](assets/screenshot9.png)
![illustration](assets/screenshot10.png)

* Right click on the element, then Reveal in Debug Navigator
![illustration](assets/screenshot11.png)

* Add the accessibility identifier
![illustration](assets/screenshot12.png)

* Build again
![illustration](assets/screenshot13.png)

* Here we are !! Identifier ready to be used ✨
  ![illustration](assets/screenshot14.png)

## Report
Test script in package.json to access to the screenshots and videos
![illustration](assets/screenshot6.png)
### Screenshots
![illustration](assets/screenshot4.png)
### Videos
![illustration](assets/screenshot7.png)
### Tests report (HTML)
With jest-html-reporters you can access to beautiful dashboard report in html
![illustration](assets/screenshot5.png)
![illustration](assets/screenshot8.png)






