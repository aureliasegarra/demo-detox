describe('Testing Wikipedia app', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should have search box', async () => {
    await element(by.text("Passer")).tap();
    await expect(element(by.text('Rechercher dans Wikipédia'))).toBeVisible();
  });


  it('should fail login', async () => {
    await element(by.label("Paramètres")).tap();
    await element(by.text("Se connecter")).tap();

    await element(by.id("qa_username")).replaceText("aureliasegarra");
    await element(by.id("qa_password")).replaceText("secret");

    await element(by.text("Se connecter")).atIndex(1).tap();
    await expect(element(by.text('Le nom d’utilisateur ou le mot de passe est incorrect.\nVeuillez essayer à nouveau.'))).toBeVisible();
  });
});
